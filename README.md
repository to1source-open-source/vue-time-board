# vue-time-board ![build](https://gitlab.com/to1source-open-source/vue-time-board/badges/master/build.svg) ![coverage](https://gitlab.com/to1source-open-source/vue-time-board/badges/master/coverage.svg)

> Vue (2+) time board display with CSS 3 animation

## Installation

Using npm with build system

```sh
$ npm install vue-time-board --save
```

or

```sh
$ yarn add vue-time-board
```

In your Vue entry js file:

```js
import vueTimeBoard from 'vue-time-board';
```

In your Vue file

```html
<template>
  <vueTimeBoard :bind:value="value" :bind:options="options"><vueTimeBoard>
</template>  
```

When you do this, your first update will see the board flip down.

### Props

There are two as seen in above example

1. value - the things you want to display
2. options - the property `letters` it's the size of boxes you want to have. If you don't supply one, we work it out by the value, but that might create a side effect that get longer or shorter based on the value. So it's advised to supply the `letters` in option (more later)

## CSS file

The CSS file is in `node_modules/vue-time-board/dist/vue-time-board.css`
So make sure you include that somewhere. Or just make a copy to your component and modify to suit your need.

## Update 1.1.0

We allow you to pass style into the tags via the `options` prop:

- wrapperStyle (default value) `padding: 0.36em; line-height: 1.3em; border-radius: 0.21em;`
- letterStyle (default value)
- textStyle (default value)

This above values should be enough for you to change the size.
If they couldn't fit your need. Then you need to overwrite the CSS file by supplying yours as well as supply the above values

## Credits

Based upon Jakub Hampl's "Designing a departures board with CSS3".
http://gampleman.eu/post/1488470623/designing-a-departures-board-with-css3

---

MIT

Developed by [to1source](https://to1source.cn) and [NEWBRAN](https://newbran.ch)
