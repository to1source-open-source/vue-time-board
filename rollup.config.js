// rollup config
import path from 'path'
import json from 'rollup-plugin-json'
import alias from 'rollup-plugin-alias'
import vue from 'rollup-plugin-vue'
import buble from 'rollup-plugin-buble'
import nodent from 'rollup-plugin-nodent'
import nodeResolve from 'rollup-plugin-node-resolve'
import commonjs from 'rollup-plugin-commonjs'
import nodeGlobals from 'rollup-plugin-node-globals'
import { uglify } from 'rollup-plugin-uglify'
import replace from 'rollup-plugin-replace'
import serverIoCore from 'rollup-plugin-server-io'
import bundleSize from 'rollup-plugin-bundle-size'
import { join, extname, basename } from 'path'
import fs from 'fs-extra'

const src = join(__dirname, 'node_modules');
const dest = join(__dirname, 'dist');

const packageJson = fs.readJsonSync('./package.json');

const isProduction = process.env.NODE_ENV === `production`
const isDevelopment = process.env.NODE_ENV === `development`

// helper to grab what to replace in different mode
const getReplaceProps = () => {
  return {
    'process.env.NODE_ENV': JSON.stringify( process.env.NODE_ENV ),
    'process_env_NODE_ENV': isProduction ? 'production' : 'development',
    'process_env_APP_VERSION': `${packageJson.version}`
  };
};

let plugins = [
  alias({
    vue$: 'vue/dist/vue.common.js',
    '@': path.resolve('./src/'),
    resolve: ['.js', '.vue', '.json']
  }),
  json(),
  vue({
    css: './dist/vue-time-board.css'
  }),
  buble({
    objectAssign: 'Object.assign'
  }),
  nodent(), // Async / await support
  nodeResolve({
    jsnext: true,
    main: true,
    browser: false
  }),
  commonjs(),
  nodeGlobals(),
  replace(getReplaceProps())
]

let config = {
  input: isProduction ? './src/dist.js' : './src/index.js',
  output: {
    file: isProduction ? './dist/vue-time-board.js' : './dev/app.js',
    format: 'umd',
    name: 'vueTimeBoard',
    sourcemap: true
  },
  plugins: plugins
}

if (isProduction) {
  config.output.sourcemap = false
  config.plugins.push(uglify())
  config.plugins.push(bundleSize())
}

if (isDevelopment) {
  config.plugins.push(serverIoCore({
    webroot: ['dev', 'src', 'node_modules']
  }))
}

export default config
