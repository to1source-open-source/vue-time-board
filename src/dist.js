import vue from 'vue';
import vueTimeBoard from './vue-time-board.vue';
import vueTimeBoardFlipper from './flipper.vue';

const Components = {
  vueTimeBoard,
  vueTimeBoardFlipper
}

Object.keys(Components).forEach(name => {
  vue.component(name, Components[name]);
});

export default Components;
