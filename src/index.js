// entry point
import vue from 'vue';
import App from './app.vue';
import './dist';

new vue({
  el: '#app',
  render: h => h(App)
});
